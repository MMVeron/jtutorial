package com.elavon.Exercise2_OOP;

public class TV {
	protected String brand;
	protected String model;
	protected boolean powerOn;
	protected int channel;
	protected int volume;

	TV(String brand, String model) {
		powerOn = false;
		channel = 0;
		volume = 5;
		this.brand = brand;
		this.model = model;
	}

	public void turnOn() {
		powerOn = true;
	}

	public void turnOff() {
		powerOn = false;
	}

	public void channelUp() {
		++channel; 
	}

	public void channelDown() {
		--channel; 
	}

	public void volumeUp() {
		++volume;
	}

	public void volumeDown() {
		--volume;
	}

	@Override
	public String toString() {
		
	return brand+" "+model+" [On:" + powerOn + ", channel:" + channel + ", volume:" + volume+"]";
	}

	public static void main(String[] args) {
		TV tv = new TV("Andre Electronics", "ONE");
		
		tv.turnOn();
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();
		tv.channelDown();
		tv.volumeDown();
		tv.volumeDown();
		tv.volumeDown();	
		tv.volumeUp();
		tv.turnOff();
		
		System.out.println(tv);

	}
}
