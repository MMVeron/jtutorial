package com.elavon.Exercise2_OOP;

public class SmartTV extends ColoredTV {

	SmartTV(String brand, String model) {
		super(brand, model);
	}

	String network;
	boolean networkStatus;

	public void connectToNetwork(String testNetwork) {
		this.network = testNetwork;
	}

	public boolean hasNetworkConnection() {

		if (network == null) {
			networkStatus = false;
		} else {
			networkStatus = true;
		}
		return networkStatus;

	}
	
	public static void main(String[] args) {
		SmartTV samsungTV = new SmartTV("SAMSUNG", "SMTV1");
		System.out.println(samsungTV);
		samsungTV.mute();
		samsungTV.brightnessUp();
		samsungTV.brightnessUp();
		samsungTV.brightnessUp();
		samsungTV.brightnessDown();
		samsungTV.brightnessDown();
		samsungTV.brightnessDown();
		samsungTV.brightnessDown();
		samsungTV.brightnessDown();
		samsungTV.contrastUp();
		samsungTV.contrastUp();
		samsungTV.contrastUp();
		samsungTV.contrastDown();
		samsungTV.contrastDown();
		samsungTV.contrastDown();
		samsungTV.contrastDown();
		samsungTV.contrastDown();
		samsungTV.pictureUp();
		samsungTV.pictureUp();
		samsungTV.pictureUp();
		samsungTV.pictureDown();
		samsungTV.channelUp();
		samsungTV.channelUp();
		System.out.println(samsungTV);
		samsungTV.connectToNetwork("TestNetwork");
		System.out.println("NETWORK NAME:" + samsungTV.network
				+ " || CONNECTED?:" + samsungTV.hasNetworkConnection());

	}

}
