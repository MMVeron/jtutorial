package com.elavon.Exercise2_OOP;

public class ColoredTV extends TV {

	int brightness;
	int contrast;
	int picture;

	ColoredTV(String brand, String model) {
		super(brand, model);
		brightness = 50;
		contrast = 50;
		picture = 50;
	}

	public void brightnessUp() {
		++brightness;
	}

	public void brightnessDown() {
		--brightness;
	}

	public void contrastUp() {
		++contrast;
	}

	public void contrastDown() {
		--contrast;
	}

	public void pictureUp() {
		++picture;
	}

	public void pictureDown() {
		--picture;
	}

	public void switchToChannel(int channel) {
		this.channel = channel;
	}

	public void mute() {
		volume = 0;
	}

	@Override
	public String toString() {
		return brand + " " + model + " [On:" + powerOn + ", channel:" + channel
				+ ", volume:" + volume + "]" + " [b:" + brightness + " c:"
				+ contrast + " p:" + picture + "]";

	}
	

	public static void main(String[] args) {
		TV bnwTV, sonyTV;
		bnwTV = new TV("Admiral","A1");
		sonyTV = new TV("SONY","S1");
		
		System.out.println(bnwTV);
		System.out.println(sonyTV);	
		
		ColoredTV sharpTV = new ColoredTV("SHARP","S1");
		sharpTV.mute();
		sharpTV.brightnessUp();
		sharpTV.brightnessUp();
		sharpTV.brightnessDown();
		sharpTV.contrastDown();
		sharpTV.contrastUp();
		sharpTV.contrastUp();
		sharpTV.pictureUp();
		sharpTV.pictureUp();
		sharpTV.pictureDown();
//		sharpTV.switchToChannel(5);
		
		System.out.println(sharpTV);
	}
}
